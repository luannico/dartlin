[![plugin version](https://img.shields.io/pub/v/dartlin?label=pub)](https://pub.dev/packages/dartlin)
[![coverage report](https://gitlab.com/wolfenrain/dartlin/badges/master/coverage.svg)](https://gitlab.com/wolfenrain/dartlin/-/commits/master)
[![pipeline status](https://gitlab.com/wolfenrain/dartlin/badges/master/pipeline.svg)](https://gitlab.com/wolfenrain/dartlin/-/commits/master)
[![dependencies](https://img.shields.io/librariesio/release/pub/dartlin?label=dependencies)](https://gitlab.com/wolfenrain/dartlin/-/blob/master/pubspec.yaml)

![Dartlin](https://i.imgur.com/zqt3wx7.png)

## Introduction

Dartlin is heavily inspired by [Kotlin](https://kotlinlang.org), therefore Dartlin tries to bring the ease of data manipulation and clean code from Kotlin to Dart.

### What it does

Dartlin is a helper library that provides the following concepts:
- Writing "cleaner" code by adding readable control-flow methods that are able to replace the standard Dart control-flow statements. See [Control flow](#control-flow) for more information.
- Adds extension methods to existing Dart types for better and easier data manipulation. See [Data manipulation](#data-manipulation) for more information.
- Provides methods to easily create data sets of certain types.

### What it does not do

Dartlin is **not** a performance library, the methods it provides exist to help the developer, and to keep their code clean and readable. Readability does not equal performance optimization, so if you are looking to optimize your code, this is not the library for you.

**Note**: Dartlin tries to keep the performance of the provided methods as good as possible but they are not being benchmarked in any way. Most of the methods rely on [closures](https://dart.dev/guides/language/language-tour#anonymous-functions) to work.

### Current libraries
Dartlin provides different libraries that you can import:
- [dartlin/collections.dart](https://pub.dev/documentation/dartlin/latest/collections/collections-library.html)
  ```dart
  import 'package:dartlin/collections.dart';
  ```
- [dartlin/control_flow.dart](https://pub.dev/documentation/dartlin/latest/control_flow/control_flow-library.html)
  ```dart
  import 'package:dartlin/control_flow.dart';
  ```
- [dartlin/text.dart](https://pub.dev/documentation/dartlin/latest/text/text-library.html)
  ```dart
  import 'package:dartlin/dartlin.dart';
  ```

Or you can use the [common library](https://pub.dev/documentation/dartlin/latest/dartlin/dartlin-library.html):

```dart
import 'package:dartlin/dartlin.dart';
```

## Common Usage

### Control flow

The Dartlin `control_flow` library provides methods for writing cleaner and more controllable code, these control flow methods can be used as a replacement for existing [control flow statements](https://dart.dev/guides/language/language-tour#control-flow-statements) or in combination with them. 

**Note**: Dart already provides a few interesting control flow operations for `List`s and `Map`s. So before you decide to use Dartlin, first read more about [collection operators](https://dart.dev/guides/language/language-tour#collection-operators). It may already suit your needs.

#### if-statement as expressions

The `iff` method works like the if statement, but with the added bonus of being able to write them as expressions. You can use them to replace complex ternary operators with a readable if-like statement:

```dart
final x = iff(a < b, () {
  return a;
}).elseIf(a == b, () {
  return 0;
}).orElse(() {
  return b;
});
```

The `iff` method is null-aware. This means that the type of the returned value from the `block` is expected as a non-nullable type for the blocks of the `elseIf` and `orElse` methods.

See the [iff](https://pub.dev/documentation/dartlin/latest/control_flow/iff.html) docs for more information.

#### switch-like statements as expressions

The `when` method replaces the switch statement. And can be used to write expressions:

```dart
final result = when(place, {
  1: () => CompetitionPlace.first,
  2: () => CompetitionPlace.second,
  3: () => CompetitionPlace.third,
  [4,5]: () => CompetitionPlace.honourableMentions,
}).orElse(() => CompetitionPlace.others);
```

See the [when](https://pub.dev/documentation/dartlin/latest/control_flow/when.html) docs for more information.

#### try-statement as expressions

The `tryy` method works like the tryy statement, but with the added bonus of being able to write them as expressions. Allowing you to catch multiple exceptions and depending on those exceptions return different values:

```dart
final x = tryy(() {
  return aMethodThatCouldFail();
}, catches: {
  On<SomeException>: () {
    return 1;
  },
  On<OtherException>: () {
    return 2;
  }
});
```

See the [tryy](https://pub.dev/documentation/dartlin/latest/control_flow/tryy.html) docs for more information.

### Data manipulation

#### Creating progression ranges

The `range` method allows the user to easily create a lists of certain `num` types. While being able to define the start value, the end value, the steps it should take to reach the end value and if it should be created in reverse or not:

```dart
// [0, 1, 2, 3, 4]
final list1 = range(0, to: 4);

// [4, 3, 2, 1, 0]
final list2 = range(4, downTo: 0); 

// [1, 3, 5, 7]
final list3 = range(1, to: 8, step: 2);

// [1, 2, 3]
final list4 = range(0, until: 4);
```

See the [range](https://pub.dev/documentation/dartlin/latest/collections/range.html) docs for more information.

# Development and Contributing
Interested in contributing? We love merge requests! See the [Contribution](https://gitlab.com/wolfenrain/dartlin/blob/master/CONTRIBUTING.md) guidelines.
