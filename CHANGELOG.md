## 0.6.3
- Added `zipWithNext` to List
- Added `equals` & `hashCode` to the `Pair` class

## 0.6.2
- Added `associateWith` to Iterable

## 0.6.1
- Added `mapIndexed` to Iterable

## 0.6.0
- **BREAKING**: `T.let` now follows the Kotlin implementation.
- **BREAKING**: `when` no longer has the `orElse` key as it was broken on web, use `T.orElse` instead.
- `iff` is now more aware of nullable types.
- Added the `T.also` extension method.
- Added the `T.takeIf` extension method.
- Added the `T.takeUnless` extension method.

## 0.5.2
- Add `getOrPut` to `Map`.

## 0.5.1
- Add `mapNotNull` and `filterNotNull` to `Iterable`.

## 0.5.0

- Stable null-safety release.
- Updated Dart SDK constraints to `>=2.12.0 <3.0.0`.

## 0.4.3

- Added `firstOrNull`, `singleOrNull`, `getOrNull` to Iterable.

## 0.4.2

- Added `range` based methods to `num` types: `downTo`, `to` and `until`.

## 0.4.1

- Added the `repeat` method.
- Added the `T.let` extension method.

## 0.4.0

- Added a `groupingBy` method for both Iterables and strings. And a `Grouping` class to accommodate it.
- Added a `chunked` method for both Iterables and strings. The returned Iterable is lazy.
- Added an `associateBy` method for both Iterables and strings.
- Added an `associate` method for both Iterables and strings.
- Added a `tryy` method to allow for assignable try-catch statements.
- **BUG FIX**: The `range` method no longer freezes up when the `step` method is not even/odd like the end value.
- **BREAKING**: The `iff` method now works for all types, not only strings. At the cost that the `elseIf` and `orElse` will also be triggered if the returned value is `null`.

## 0.3.0

- **BREAKING**: Renamed the `ranges` library to `collections`.

## 0.2.0+1

- Added `iff` documentation.

## 0.2.0

- Added `iff` method for control flow.
- **BREAKING**: Removed the `when` simplified if-else chain, use the new `iff`.

## 0.1.0+1

- Fixed issues reported in the pub score.

## 0.1.0

- Added `when` method for control flow.
- Added `range` method for ranges.
