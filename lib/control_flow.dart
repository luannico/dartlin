/// Adding control flow methods for writing more readable code.
library control_flow;

part 'src/control_flow/also.dart';
part 'src/control_flow/iff.dart';
part 'src/control_flow/let.dart';
part 'src/control_flow/take_if.dart';
part 'src/control_flow/take_unless.dart';
part 'src/control_flow/tryy.dart';
part 'src/control_flow/when.dart';
part 'src/control_flow/when_checks.dart';
