/// Adding methods for collection control.
library collections;

import 'dart:collection';

part 'src/collections/associate.dart';
part 'src/collections/get_or_null.dart';
part 'src/collections/get_or_put.dart';
part 'src/collections/grouping_by.dart';
part 'src/collections/chunked.dart';
part 'src/collections/filter_not_null.dart';
part 'src/collections/first_or_null.dart';
part 'src/collections/map_not_null.dart';
part 'src/collections/map_indexed.dart';
part 'src/collections/range_num.dart';
part 'src/collections/range.dart';
part 'src/collections/repeat.dart';
part 'src/collections/single_or_null.dart';
part 'src/collections/zip_with_next.dart';
